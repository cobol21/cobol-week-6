       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. NitroEz.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT  EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  EMP-FILE.
       01 EMP-DETAILS.
          88 END-OF-EMP-FILE              VALUE HIGH-VALUE.
       05 EMP-SSN               PIC 9(9).
          05 EMP-NAME.           
             10 EMP-SURNAME     PIC X(15).
             10 EMP-FORENAME    PIC X(10).
          05 EMP-DATE-OF-BIRTH.
             10 EMP-YOB         PIC 9(4).
             10 EMP-MOB         PIC 9(2).
             10 EMP-DOB         PIC 9(2).
          05 EMP-GENDER         PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE 
           MOVE "123456789" TO EMP-SSN 
           MOVE "CHANPREM" TO EMP-FORENAME 
           MOVE "KITTIPICH" TO EMP-SURNAME  
           MOVE "20001117" TO EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           MOVE "987654321" TO EMP-SSN 
           MOVE "EZ" TO EMP-FORENAME  
           MOVE "Nitro" TO EMP-SURNAME 
           MOVE "20001117" TO EMP-DATE-OF-BIRTH 
           MOVE "M" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           MOVE "2746314633" TO EMP-SSN 
           MOVE "SRISOMPONG" TO EMP-FORENAME  
           MOVE "YANISA" TO EMP-SURNAME 
           MOVE "20000316" TO EMP-DATE-OF-BIRTH 
           MOVE "F" TO EMP-GENDER 
           WRITE EMP-DETAILS 

           CLOSE EMP-FILE 
           GOBACK 
           .